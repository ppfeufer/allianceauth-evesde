import csv
import email
import inspect
import os
from datetime import datetime
from unittest.mock import Mock, patch

from django.test import TestCase
from django.utils.timezone import make_aware, now, utc

from .. import tasks
from ..models import EveRace
from ..utils import make_logger_prefix, set_test_logger

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

MODULE_PATH = "evesde.tasks"
logger = set_test_logger(MODULE_PATH, __file__)


def patch_fetch_csv_file_from_server(table_name: str, download_url: str, addTag):
    f_csv = open(os.path.join(currentdir, "testdata/{}.csv".format(table_name)), "r")
    return "test_hash", now(), f_csv


class TestTasks(TestCase):

    """

    @classmethod
    def setUpClass(cls):
        super(TestTasks, cls).setUpClass()

    """

    def setUp(self):
        tasks.purge_all_data(i_am_sure=True)

    @patch(MODULE_PATH + ".requests.get")
    def test_fetch_sde_table_from_server(self, mock_requests_get):
        # create test data and expected results
        with open(os.path.join(currentdir, "testdata/chrRaces.csv.bz2"), "rb") as f:
            testfile = f.read()

        with open(os.path.join(currentdir, "testdata/chrRaces.csv"), "r") as f:
            expected_data = list(csv.DictReader(f, delimiter=","))

        # create mocks
        my_last_modified = make_aware(datetime(2020, 1, 15, 12, 30), utc)
        mock_response = Mock()
        mock_response.content = testfile
        mock_response.raise_for_status = Mock()
        mock_response.headers = {
            "last-modified": email.utils.format_datetime(my_last_modified)
        }
        mock_requests_get.return_value = mock_response

        # run test
        data_hash, last_modified, f_csv = tasks._fetch_csv_file_from_server(
            "x", "y", make_logger_prefix("testing")
        )

        # checking test results
        self.assertEqual(last_modified, my_last_modified)
        received_data = list(csv.DictReader(f_csv, delimiter=","))
        self.assertListEqual(received_data, expected_data)

    def test_convert_camel_to_snake(self):
        self.assertEqual(
            tasks._convert_camel_to_snake("camel2_camel2_case"), "camel2_camel2_case"
        )
        self.assertEqual(
            tasks._convert_camel_to_snake("getHTTPResponseCode"),
            "get_http_response_code",
        )
        self.assertEqual(
            tasks._convert_camel_to_snake("HTTPResponseCodeXYZ"),
            "http_response_code_xyz",
        )

    @patch(MODULE_PATH + "._fetch_csv_file_from_server")
    def test_run_update_one_model(self, mock_fetch_csv_file_from_server):
        mock_fetch_csv_file_from_server.side_effect = patch_fetch_csv_file_from_server

        def _none_2_none(d: dict) -> dict:
            for k, v in d.items():
                if v == "None":
                    d[k] = None
            return d

        tasks.run_update(
            model_to_update="EveRace",
            force_update=True,
            report_mode=tasks.REPORT_MODE_ERRORS,
        )
        with open(os.path.join(currentdir, "testdata/chrRaces.csv"), "r") as f:
            csv_reader = csv.DictReader(f)
            expected_data_list = {x["raceID"]: _none_2_none(x) for x in csv_reader}

        for pk, expected_data in expected_data_list.items():
            obj = EveRace.objects.get(pk=pk)
            self.assertEqual(obj.race_name, expected_data["raceName"])
            self.assertEqual(obj.description, expected_data["description"])
            self.assertEqual(
                obj.icon_id,
                int(expected_data["iconID"])
                if expected_data["iconID"]
                else expected_data["iconID"],
            )
            self.assertEqual(obj.short_description, expected_data["shortDescription"])

    @patch(MODULE_PATH + "._fetch_csv_file_from_server")
    def test_run_update_complete(self, mock_fetch_csv_file_from_server):
        # create mocks

        mock_fetch_csv_file_from_server.side_effect = patch_fetch_csv_file_from_server

        tasks.run_update(force_update=True, report_mode=tasks.REPORT_MODE_ERRORS)

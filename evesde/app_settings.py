from django.conf import settings

# URL to download the CSV files containing the Eve Online SDE from
# This can be overwritten to download files from another web server if needed
# e.g. a local copy in case if the default server is temporarily unavailable
EVESDE_DOWNLOAD_SERVER_URL = getattr(
    settings, "EVESDE_DOWNLOAD_SERVER_URL", "https://www.fuzzwork.co.uk/dump/latest/"
)

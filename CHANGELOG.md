# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] - yyyy-mm-dd

### Added

### Changed

### Fixed

## [0.7.3] - 2021-03-15

### Changed

- Improved tests
- Adopted CI for Auth 2.8.2

## [0.7.2] - 2020-03-03

### Changed

- New squashed migrations incl. old migrations

# allianceauth-evesde

Eve Online static data for Alliance Auth

[![pipeline](https://gitlab.com/ErikKalkoken/allianceauth-evesde/badges/master/pipeline.svg)](https://gitlab.com/ErikKalkoken/allianceauth-evesde/-/pipelines)
[![coverage](https://gitlab.com/ErikKalkoken/allianceauth-evesde/badges/master/coverage.svg)](https://gitlab.com/ErikKalkoken/allianceauth-evesde/-/pipelines)
[![license](https://img.shields.io/badge/license-MIT-green)](https://gitlab.com/ErikKalkoken/allianceauth-evesde/-/blob/master/LICENSE)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![chat](https://img.shields.io/discord/790364535294132234)](https://discord.gg/zmh52wnfvM)

## Description

evesde ia plugin for [Alliance Auth](https://gitlab.com/allianceauth/allianceauth) that provides a local copy of Eve Online static data exchange tables (SDE) as Django models and with data. An automatic update process keeps the local copy current.

The main benefit of having a local copy of the SDE is the huge performance improvement over requesting data on demand from ESI and the ability to harmonize data models using Eve data across apps.

## Features

- Provides a local copy of the Eve Online SDE (subset) as Django models

- Automatically updates local copy once new data becomes available

## Installation

1. Install into AA virtual environment with PIP install from this repo

   ```bash
   pip install git+https://gitlab.com/ErikKalkoken/allianceauth-evesde
   ```

2. Configure your AA settings (`local.py`)
   - Add `'evesde'` to `INSTALLED_APPS`
   - Add these lines:

   ```python
   CELERYBEAT_SCHEDULE['evesde.run_regular_update'] = {
    'task': 'evesde.tasks.run_update',
    'schedule': crontab(minute=0, hour=3),
    'kwargs': {'report_mode': 'events'}
   }

   ```

   > **Note**:<br>This configures the update process to run every night at 03:00 AM UTC and only report when data in the DB was updated or on errors. The AA server is configured for UTC timezone by default. Make sure to adjust this settings to your particular timezone so that it runs during off hours.

3. Run migrations & restart AA, Celery, Gunicorn

4. Run the initial update for all models:

```bash
celery -A myauth call evesde.tasks.run_update
```

> **IMPORTANT**:<br>The initial update can take between 30 - 60 minutes to complete. Once the update process is finished you will get a notification.<br>AA can be used normally by your alliance during the update process, but DO NOT install any additional apps that depend on evesde before the initial update is complete.

> **Note**:<br>Change "myauth" to the name of your AA project as needed.

## Admin page

The basic admin functions can be accessed conveniently On the admin page you have the following functionality:

- Check when a model was last updated
- Run update process for all models
- Run update process for one model

## Command line

For access to all admin functions you can call the respective celery tasks directly from the command line.

The syntax for starting a celery task is:

```plain
celery -A PROJECT call TASK [--kwargs='ARGUMENTS']
```

> **Note**:<br>ARGUMENTS is a key-value array in JSON.

Example:

If your AA project (PROJECT) is called myauth, then you can start the update process for the EveFaction model as follows:

```bash
celery -A myauth call evesde.tasks.run_update --kwargs='{"model_to_update": "EveFaction"}''
```

Here is a complete list of all tasks and their arguments

### Task: Run update

#### Name

`evesde.tasks.run_update`

#### Description

Manually starts the update process

#### Arguments

Name | Default | Example Value | Description
-- | -- | -- | --
`"force_update"` | `false` | `true` |  Run a complete update of all models even if the respective SDE tables on the download server have not changed
`"model_to_update"` | `null` | `"EveFaction"` | Run update for this model only. Careful: Trying to update single models can lead to foreign key conflicts and should only be done for troubleshooting (e.g. to retry a failed update of one model)
`"download_url"` | `null` | `"https://www.fuzzwork.co.uk/dump/sde-20190529-TRANQUILITY/"` | update will use the provided url instead of the configured one. This can be useful if the update from the official download URL is not working.
`"report_mode"` | `"full"` | `"events"` |  Defines when reports are sent to admins:<br> `"full"`: always report result of update process, report errors<br>`"events"`: report only if a model was updated and on errors<br> `"errors"`: only report errors<br> `"quiet"`: disable all reports

### Task: Purge all data

#### Name

`evesde.tasks.purge_all_data`

#### Description

This task deletes all data from all models in the DB. This feature is provided for troubleshooting purposed only and will most likely create integrity conflicts with existing models of other applications. USE WITH CARE

#### Arguments

Name | Default | Example Value | Description
-- | -- | -- | --
`"i_am_sure"` | `null` | `true` |  Needs to be set true to run the purge

## Logging

Most logging is on the INFO level. If you want to monitor activity of the evesde tasks make sure that your workers are configured to log at least on the INFO level (default if WARNING) in your `supervisor.conf`.

Example for configuring celery workers  for info level logging:

```plain
celery -A myauth worker -l info
```

The log file for workers is in `path/to/myauth/log/worker.log`

## Integration with other apps

tbd.
